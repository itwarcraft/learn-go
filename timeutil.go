﻿package main

import (
	"time"
	"fmt"
)

const(
	YYYY_MM_DD = "yyyy-MM-dd"
	LAYOUT_YYYY_MM_DD = "2006-01-02"
	YYYY_M_D = "yyyy-M-d"
	LAYOUT_YYYY_M_D = "2006-01-02"
)

func Format2Str(layout string,t *time.Time)(string,error){
	if layout==YYYY_MM_DD{
		return time.Format(LAYOUT_YYYY_MM_DD,t)
	}
}

func main(){
	str,err:=Format2Str("yyyy-MM-dd",&time.Now())
	if err!=nil{
		fmt.Println("error...")
		fmt.Println(err)
	}
	fmt.Printf("str=%s\n",str)
}